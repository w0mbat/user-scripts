# user-scripts

## for AtCoder

- [View editorials of all problems in one page.](https://gitlab.com/w0mbat/user-scripts/-/raw/main/js/editorial-collector.user.js)
- [Add checkboxes to constraints of an AtCoder problem.](https://gitlab.com/w0mbat/user-scripts/-/raw/main/js/check-constraints.user.js)
